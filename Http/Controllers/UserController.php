<?php

namespace Modules\Users\Http\Controllers;

use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\AdminCore\Http\Controllers\AdminResourceController;
use Modules\Users\Entities\User;
use Modules\Users\ModelServices\UserService;

class UserController extends AdminResourceController
{
    protected $resource_model = User::class;

    protected $prefix = 'users::';

    /**
     * @var UserService
     */
    protected $service;

    /**
     * UserController constructor.
     * @param UserService $user_service
     * @throws Exception
     */
    public function __construct(UserService $user_service)
    {
        parent::__construct();

        $this->service = $user_service;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $user = $this->service->create($request->toArray());

        return $this->redirectToEdit($user);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $this->service->update($user, $request->toArray());

        if ($request->has('password'))
        {
            $this->service->changePassword($user, $request->input('password'));
        }

        return $this->redirectToIndex();
    }
}
