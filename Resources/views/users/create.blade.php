@extends('admincore::layouts.master')

@section('content')

    <h4>Nový uživatel</h4>

    <div class="row">
        <div class="col-6">
            <form method="post"
                  action="{{$route_store}}">
                @csrf

                <div class="form-group">
                    <label>Jméno</label>
                    <input type="text"
                           class="form-control"
                           name="name"
                           required>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text"
                           class="form-control"
                           name="email"
                           required>
                </div>

                <div class="form-group">
                    <label>Heslo</label>
                    <input type="password"
                           class="form-control"
                           name="password" min="8">
                </div>

                <input type="submit"
                       class="btn btn-primary"
                       value="Uložit záznam">
            </form>
        </div>
    </div>
@endsection
