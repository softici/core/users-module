@extends('admincore::layouts.master')

@section('content')

    <h4>Úprava uživatele</h4>

    <div class="row">
        <div class="col-6">
            <form method="post"
                  action="{{$route_update}}">
                @csrf
                @method('PATCH')

                <div class="form-group">
                    <label>Jméno</label>
                    <input type="text"
                           class="form-control"
                           name="name"
                           required value="{{$user->name}}">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text"
                           class="form-control"
                           name="email"
                           required value="{{$user->email}}">
                </div>

                <div class="form-group">
                    <label>Oprávnění</label>
                    <select class="form-control"
                            name="role">
                        <option {{ $user->role == 'user' ? 'selected' : '' }} value="user">překladatel</option>
                        <option {{ $user->role == 'moderator' ? 'selected' : '' }} value="moderator">překladatel s
                            oprávněním schvalovat
                            překlady
                        </option>
                        <option {{ $user->role == 'admin' ? 'selected' : '' }} value="admin">administrátor</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Heslo</label>
                    <input type="password"
                           class="form-control"
                           name="password"
                           min="8">
                    <small>Pro zachování hesla ponechte prázdné.</small>
                </div>

                <input type="submit"
                       class="btn btn-primary"
                       value="Uložit záznam">
            </form>
        </div>
    </div>
@endsection
