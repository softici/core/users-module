@extends('admincore::layouts.master')

@section('content')

    @include('admincore::components.cdn.datatable')

    <a class="btn btn-primary float-right"
       href="{{$route_create}}">Nový uživatel</a>

    <h2>Uživatelé</h2>

    <table class="table"
           style="font-size: 13px"
           id="table-quotes">
        <thead>
        <tr>
            <th>Jméno</th>
            <th>Email</th>
            <th>Role</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    <a href="{{route('users::users.edit', $user)}}">{{$user->name}}</a>
                </td>
                <td>{{$user->email}}</td>
                <td>{{$user->role}}</td>
                <td>{{$user->getRoleName()}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
