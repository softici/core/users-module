<?php


namespace Modules\Users\ModelServices;


use Exception;
use Modules\Users\Entities\User;

class UserService
{
    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        $user = new User();

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return User
     */
    public function update(User $user, array $data): User
    {
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->save();

        return $user;
    }

    /**
     * Change user password.
     *
     * @param User $user
     * @param $password
     * @return User
     */
    public function changePassword(User $user, $password): User
    {
        $user->password = bcrypt($password);
        $user->save();

        return $user;
    }

    /**
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public function delete(User $user): bool
    {
        $user->delete();

        return true;
    }
}
